import chalk from 'chalk';

const message: string = 'Built with Babel';

console.log(chalk.black.bgGreenBright(message));
